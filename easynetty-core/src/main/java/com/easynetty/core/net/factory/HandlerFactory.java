package com.easynetty.core.net.factory;

import io.netty.channel.ChannelHandler;

/**
 * Created by Mengyue on 2016-07-22.
 */
public interface HandlerFactory {
    ChannelHandler getHandler();
}
