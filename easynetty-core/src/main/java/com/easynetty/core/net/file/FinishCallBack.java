package com.easynetty.core.net.file;


import com.easynetty.core.net.entity.FileInfo;

@FunctionalInterface
public interface FinishCallBack {
    public void accept(FileInfo f);
}
