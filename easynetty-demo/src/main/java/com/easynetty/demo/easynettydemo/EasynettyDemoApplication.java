package com.easynetty.demo.easynettydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasynettyDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasynettyDemoApplication.class, args);
	}
}
