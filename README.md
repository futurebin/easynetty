## EasyNetty是什么?
一个基于netty封装的快速分布式任务开发框架，目标：简单，简单，再简单！


## EasyNetty有哪些功能？

* 方便的`服务-客户端`功能
    *  封装netty服务和客户端的启动,很简单
    *  简单的自定义协议
* 可快速实现任务消息的分发
* 可快速实现超大文件传输，无内存占用
* 可快速实现文件断点续传功能


## 快速开始
+ easynetty-demo是用spring-boot，spring-shell实现的示例
+ [点击下载](http://files.git.oschina.net/group1/M00/02/7C/PaAvDFot6EmADIFSAO1pnBhj8IQ187.jar?token=9b5b3f5354f975bb0ce4c71472c70af9&ts=1512958141&attname=easynetty-demo-0.0.1-SNAPSHOT.jar)
+ 切换到下载文件目录，执行 以下命令
```
java -jar easynetty-demo-0.0.1-SNAPSHOT.jar
start
send 'yourfilename'
```
+ yourfilename为本地电脑文件路径 路径分隔使用“\\\”
+ 文件会默认传输到d:\\\serverFile
![](http://chuantu.biz/t6/172/1512958379x-1404755502.gif)